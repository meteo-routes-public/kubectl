FROM alpine:3.18
ARG VERSION
RUN apk add --no-cache curl \
    && curl -LO "https://dl.k8s.io/release/$VERSION/bin/linux/amd64/kubectl" \
    && chmod +x kubectl \
    && mv kubectl /usr/local/bin/ \
    && apk del --purge curl
